import React from 'react';
import { Redirect, Route, Switch } from "react-router"
import { Link } from 'react-router-dom';


export const HomePage = (props) => {

    return (
    <div>
      <nav className="topnav">
        <ul>
          <li>
            <Link className="link" to="/select">Search</Link>
          </li>
          <li>
            <Link className="link" to="/selected">Selected</Link>
          </li>
          <li>
            <Link className="link" to="/login">Logout</Link>
          </li>
        </ul>
      </nav>
      <Switch>
        <Route path="/select">
            <h2>SELECT</h2>
        </Route>
        <Route path="/selected">
            <h2>SELECTED</h2>
        </Route>
        <Route>
            <Redirect to="select" />
        </Route>
      </Switch>
    </div>
    );
}