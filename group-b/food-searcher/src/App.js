import React from 'react';
import './App.css';

import { Route, Switch } from 'react-router';

import { HomePage } from './component/HomePage/HomePage';

const App = (props) => {


  return (
    <div className="App">
      <Switch>
        <Route path="/login">
          <h2>Login Page</h2>
        </Route>
        <Route path="/">
          <h1>Food Searcher</h1>
          <HomePage />
        </Route>
      </Switch>
    </div>
  );

  
}

export default App;
